﻿---

## About

The purpose of this project is to let you familiarise on how to configure a Modbus RTU communication to TwinCAT.

---


## Pre-requisite

If you are staff of Beckhoff, you can use NovoCon s CO6, Energy, IO - High Accuracy Actuator in our office.

There is no installation of driver required, you just need to do mapping of variables in TwinCAT.

```
You can follow the "Modbus RTU Guide" for better understanding.
```


---

## How to use

You need to know what is the modbus function that you are using, refer to https://infosys.beckhoff.com/english.php?content=../content/1033/tcplclibmodbusrtu/455276555.html&id=2856145827112356268 to know whether you can use ReadCoils or WriteSingleCoil and etc function blocks.


---

## Help

Contact support@beckhoff.com.sg

---

## Styling syntax

[Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[Markdown preview](https://dillinger.io/)




